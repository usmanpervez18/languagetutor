package sms.app

import grails.converters.JSON


class AuthInterceptor {
    AuthInterceptor() {
        match( controller: "support" )
    }


    boolean before() {
        if (!params.'password') {

            render ([code: -1, desc: "Parameter password not found."]) as JSON
            return

        } else if (!params.msisdn) {

            render ([code: -1, desc: "Parameter msisdn not found."]) as JSON
            return

        } else {
            if (authenticateUser(params['password'].toString()) &&
                    validateMsisdn(params['msisdn'].toString()) )
            {
                return true
            } else {
                render([result: 401, desc: "Authentication failed."]) as JSON
            }
        }
    }

    boolean after() { true }

    void afterView() {
        // no-op
    }

    private boolean authenticateUser(String password) {
        return password.equals("eC0nc3ptaPY3?dLurhv5")
    }

    private boolean validateMsisdn(String msisdn){
        return msisdn.isLong()
    }

}
