package sms.app

import econ.api.IStatsCollector
import kk.Subscription
import org.springframework.beans.factory.annotation.Autowired
import sms.SubscriptionService

class BilledController {
    SubscriptionService subscriptionService

    @Autowired
    IStatsCollector iStatsCollector

    final int billedStat =  3001

    def index() { }
    def user() {
        if (params?.id && params?.password == '56XyCTnFuRZYKfz'){
            Long msisdn = params?.id.toLong()
            iStatsCollector.increment(billedStat)
            Subscription subscription = subscriptionService.getSubscription(msisdn)
            if(subscription){
                subscription.updateLastBillReceived()
                subscription.save( flush:true)
            }
        }
        render "DONE"
        return null

    }
}
