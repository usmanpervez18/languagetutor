package sms.app

import econ.api.IStatsCollector
import econ.api.SmsSender
import grails.converters.JSON
import kk.Subscription
import org.springframework.beans.factory.annotation.Autowired
import sms.SubscriptionService

import static content.sms.ActualCodes.getSubFreeCode

class SupportController {

    SubscriptionService subscriptionService
    @Autowired
    SmsSender smsSender

    @Autowired
    IStatsCollector iStatsCollector

    final int unsubAllStats = 9000

     def subscribe() {
        Subscription subscription = subscriptionService.getSubscription(params?.msisdn.toLong())
        final int defaultTo = (int)subFreeCode

        if (!subscription){
            if (subscriptionService.createSubscription(params?.msisdn.toLong(),'IVR')){
                SubscriptionSetting subscriptionSetting = subscriptionService.getSubscriptionSetting(params?.msisdn.toLong())
                log.info("External Sub:${params?.msisdn}")
                smsSender.sendSMS( defaultTo , params?.msisdn.toLong() , subscriptionService.msg("welcome.ivr",subscriptionSetting.lang ))
                render([code:"0",desc:"User Subscribed."]) as JSON
            } else {
                render([code:"-1",desc:"Internal Error."]) as JSON
            }
        } else {
            render ([code:"-1",desc:"Subscriber Already Subscribed."]) as JSON
        }
    }

    def unSubscribe() {
        Subscription subscription = subscriptionService.getSubscription(params?.msisdn.toLong())
        final int defaultTo = (int)subFreeCode

          if (subscription){
              log.info("External UnSub:${params?.msisdn}")
              if (subscriptionService.unSubscription(params?.msisdn.toLong())){
                  SubscriptionSetting subscriptionSetting = subscriptionService.getSubscriptionSetting(params?.msisdn.toLong())
                  smsSender.sendSMS( defaultTo , params?.msisdn.toLong() , subscriptionService.msg("unsub.success.ivr",subscriptionSetting.lang ))
                  render ([code:"0",desc:"Subscriber UnSubscribed."]) as JSON
              } else {
                  render ([code:"-1",desc:"Internal Error."]) as JSON
              }
          } else {
              render ([code:"-1",desc:"Subscriber Not Found."]) as JSON
          }
    }

    def applyJob() {
        Subscription subscription = subscriptionService.getSubscription(params?.msisdn.toLong())
        SubscriptionSetting subscriptionSetting = subscriptionService.getSubscriptionSetting(params?.msisdn.toLong())
        Map<String,String> resList = ["Already Applied":"job.already.applied",
                                  "Successfully Applied":"job.successfully.applied",
                                  "Profile Not Complete":"job.profile.not.complete",
                                  "Job Not Exists":"job.not.exists"]
        final int defaultTo = (int)subFreeCode

        if (subscription){
            Long jobId = params?.jobId.toLong()
            log.info("External Apply Job:${params?.jobId}")
            String res = subscriptionService.applyJob(jobId,params?.msisdn.toLong())
            if (res != ""){
                //smsSender.sendSMS( defaultTo , params?.msisdn.toLong() , subscriptionService.msg(resList.get(res),subscriptionSetting.lang ))

                render ([code:"0",desc:"${res}"]) as JSON
            } else {
                render ([code:"-1",desc:"Internal Error."]) as JSON
            }
        } else {
            render ([code:"-1",desc:"Subscriber Not Found."]) as JSON
        }
    }

    def subscribeCategory(){
        Subscription subscription = subscriptionService.getSubscription(params?.msisdn.toLong())
        SubscriptionSetting subscriptionSetting = subscriptionService.getSubscriptionSetting(params?.msisdn.toLong())
        Map<String,String> resList = ["Category Already Sub":"category.already.sub",
                                      "Category Subscription Successful":"category.subscription.successful"]
        final int defaultTo = (int)subFreeCode

        if (subscription){
            int catId = params?.catId.toInteger()
            log.info("External Category Subscription:${params?.catId}")
            String res = subscriptionService.categorySubscribe(params?.msisdn.toLong(),catId)
            if (res != ""){
                //smsSender.sendSMS( defaultTo , params?.msisdn.toLong() , subscriptionService.msg(resList.get(res),subscriptionSetting.lang,[Categories.get(catId)] ))

                render ([code:"0",desc:"${res}"]) as JSON
            } else {
                render ([code:"-1",desc:"Internal Error."]) as JSON
            }
        } else {
            render ([code:"-1",desc:"Subscriber Not Found."]) as JSON
        }
    }

    def sendSms() {
        Subscription subscription = subscriptionService.getSubscription(params?.msisdn.toLong())
        if (subscription){
            log.info("External Send SMS ${params?.msisdn}:${params?.text}")
            final int defaultTo = (int)subFreeCode

            if (smsSender.sendSMS( defaultTo , params?.msisdn.toLong() , params?.text )){
                render ([code:"0",desc:"SMS Sent."]) as JSON
            } else {
                render ([code:"-1",desc:"Internal Error."]) as JSON
            }
        } else {
            render ([code:"-1",desc:"Subscriber Not Found."]) as JSON
        }
    }

    def unsubAll = {
        Map result = [:]

        try{
            long msisdn = params.msisdn.toLong()

            Subscription subscription = Subscription.findById(msisdn)
            if (!subscription){
                result = [ result:2 , desc:'User not found']
            } else {
                try {
                    subscription.delete()

                    try {
                        String response = subscriptionService.parseResponse(subscriptionService.doPost("d", msisdn.toString()))
                        if (response?.trim()?.startsWith("1")) {
                            log.error("Zong UnSubAll Intimation Success: [msisdn:${msisdn}] [response:${response}]")
                        } else {
                            log.error("Zong UnSubAll Intimation Failed: [msisdn:${msisdn}] [response:${response}]")
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace()
                        log.error("Zong UnSubAll Intimation Failed: [msisdn:${msisdn}] [exception: ${ex.getMessage()}]")
                    }

                    iStatsCollector.increment(unsubAllStats)
                    log.info("Rozgar UNSUBALL : $msisdn ${new Date().format('yyyy-MM-dd HH:m:s')}")
                    result = [ result:1 , desc:'User unsubscribed Successfully']

                } catch (Exception e) {
                    result = [ result:-1 , desc:'Internal error']
                    e.printStackTrace()
                }
            }

        }catch (Exception ex){
            ex.printStackTrace()
            result = [ result:-1 , desc:'Internal error']
        }
        render( result as JSON )
    }

}
