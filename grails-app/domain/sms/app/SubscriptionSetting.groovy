package sms.app

class SubscriptionSetting {

    String lang = 'en'
    String name = ""
    int experience
    int qualification
    int passport
    Date dateCreated
    Date lastUpdated
    int lastNotifyId = 0

    static mapping = {
        id generator:"assigned"
    }


    static constraints = {
    }
}
