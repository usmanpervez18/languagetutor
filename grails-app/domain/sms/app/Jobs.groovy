package sms.app

import kk.Subscription

class Jobs {
    String title
    String description

    static hasMany = [subscriptions:Subscription]
    static constraints = {
    }
}
