package kk

class Categories {
	String title
	String mappedOn
	int sortOrder = 0
	Categories parent

	@Override
	String toString() {
		return "$title"
	}

    static constraints = {
	    title(nullable: false, blank: false)
	    mappedOn(nullable: false, blank: false)
	    parent(nullable: true)
    }
}
