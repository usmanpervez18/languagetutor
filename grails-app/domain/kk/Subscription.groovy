package kk

import content.sms.Durations


class Subscription {

    String password = ""
    int created = Durations.currentSecond

    int nextLLNotifyTime = Durations.currentSecond + Durations.NEWSUBDELAY
    int nextDCNotifyTime = Durations.currentSecond + Durations.NEWSUBDELAY
    int lastNotifyId = 0
    Date dateCreated

    int billingSent = 0
    int billingReceived = Durations.currentSecond
//    int notificationsValidity = Durations.getServiceValidity

    int instantNotifyId = 0
    int isActive = 1
    int isComplete = 0
    String subType = ''


    public int lastBillTime() {
        return billingSent
    }

    public void updateLastBillReceived() {
        billingReceived = Durations.currentSecond;
    }

    public void updateLastBillSent() {
        billingSent = Durations.currentSecond
    }

    public void updateNextLLNotificationTime(boolean smsSent) {
        int current = Durations.currentSecond
        if (smsSent) {
            nextLLNotifyTime = current + Durations.SMSGAP
        } else {
            nextLLNotifyTime = current + Durations.NOOPDELAY
        }
    }

    public void updateNextDCNotificationTime(boolean smsSent) {
        int current = Durations.currentSecond
        if (smsSent) {
            nextDCNotifyTime = current + Durations.SMSGAP
        } else {
            nextDCNotifyTime = current + Durations.NOOPDELAY
        }
    }

    public void updateLastSmsSent() {
        lastNotifyId = Durations.currentSecond;
    }

    static mapping = {
        id generator: "assigned"
    }


    static constraints = {
        password(blank: true, nullable: true)
    }

    public static List getNotifiableLLSubscribers(int count) {
        //return Subscription.findAllByNextLLNotifyTimeLessThanEqualsAndBillingReceivedGreaterThan(Durations.currentSecond,Durations.today,[max:count, sort: "nextNotify"])
        return Subscription.findAll() 
    }

    public static List getNotifiableDCSubscribers(int count) {
        //return Subscription.findAllByNextDCNotifyTimeLessThanEqualsAndBillingReceivedGreaterThan(Durations.currentSecond,Durations.today,[max:count, sort: "nextNotify"])
        return Subscription.findAll()
    }
}
