package kk

class Lecture {

    String title;
    Integer lectureNumber;
    String language;

    static constraints = {
        title(blank: false)
        lectureNumber(nullable: false, min: 1)
        language(blank: false, nullable: false) 
    }

    static def getConcatenatedLectures(List<Lecture> lectures, Integer page) {
        def lecturesStr = ""

        for (int i = 0; i < lectures.size(); i++) {
            lecturesStr += "\n${lectures[i].lectureNumber}. " + lectures[i].title + " (Lecture ${lectures[i].lectureNumber})"
        }

        if (page >= 2) {
            lecturesStr += "\nReply with 'P' for previous lectures\nReply with 'L' for more lectures\nReply with 'M' to go to Menu"
        } else {
            lecturesStr += "\nReply with 'L' for more lectures\nReply with 'M' to go to Menu"
        }

        return lecturesStr
    }

    static def getLectures(String language = 'English', Integer maxResults = 5, Integer offset = 0) {
        return Lecture.findAllByLanguage(language, [max: maxResults, sort: "lectureNumber", order: "asc", offset: offset])
    }

    static def getLectureIdsByLanguage(String language) {
        def lecturesByLang = Lecture.findAllByLanguage(language);

        List<String> lectureIds = new ArrayList<>()

        for (lecture in lecturesByLang) {
            lectureIds.add(lecture.lectureNumber.toString())
        }

        return lectureIds
    }
}
