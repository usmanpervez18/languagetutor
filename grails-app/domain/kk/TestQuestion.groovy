package kk

class TestQuestion {

    String question
    Integer questionNumber
    String option1
    String option2
    String option3
    String option4
    Integer correctOption
    String language

    static constraints = {
        question(blank: false, nullable: false)
        option1(blank: false, nullable: false)
        option2(blank: false, nullable: false)
        option3(blank: false, nullable: false)
        option4(blank: false, nullable: false)
        correctOption(inList: [1, 2, 3, 4], nullable: false)
        language(inList: ['English', 'French', 'Arabic', 'Spanish'], nullable: false)
    }

    def static getTestQuestion(String language, Integer questionNumber) {
        return TestQuestion.findByLanguageAndQuestionNumber(language, questionNumber)
    }

    def static getFormattedQuestion(TestQuestion question) {
        String q = null
        if (question) {
            q = question.question + "\n1. " + question.option1 + "\n2. " + question.option2 + "\n3. " + question.option3 + "\n4. " + question.option4
        }

        return q
    }

    def getCorrectOptionValue() {
        switch (this.correctOption) {
            case 1:
                return this.option1
            case 2:
                return this.option2
            case 3:
                return this.option3
            case 4:
                return this.option4
        }
    }
}
