package kk


class SubscriptionProfile {

    Long msisdn
    String language
    String difficultyLevel
    Boolean learnLanguageSub
    Boolean dailyCoachingSub
    Date dateCreated

    final static def languages = ['English', 'French', 'Arabic', 'Spanish']

    static constraints = {
        msisdn(nullable: false)
        language(blank: false, nullable: false, inList: ['English', 'French', 'Arabic', 'Spanish'])
        difficultyLevel(inList: ['Basic', 'Intermediate', 'Advanced'])
        learnLanguageSub(nullable: false)
        dailyCoachingSub(nullable: false)
    }

    static Boolean subscribedToMultipleLanguages(Long msisdn) {
        return SubscriptionProfile.countByMsisdn(msisdn) > 1
    }

    String getSubscribedLanguages() {
        def selectedLang = "";
        int i = 1;
        def profiles = SubscriptionProfile.findAllByMsisdn(this.msisdn)

        for (profile in profiles) {
            selectedLang += "\n${i++}. " + profile.language
        }

        return selectedLang
    }

    static String getSubscribedLanguages(Long msisdn) {
        def selectedLang = "";
        int i = 1;
        def profiles = SubscriptionProfile.findAllByMsisdn(msisdn)

        for (profile in profiles) {
            selectedLang += "\n${i++}. " + profile.language
        }

        return selectedLang
    }

    def subscribeToLearnLanguage() {
        this.learnLanguageSub = true;
        this.save(flush: true);
    }

    def unsubscribeToLearnLanguage() {
        this.learnLanguageSub = false;
        this.save(flush: true);
    }

    def subscribeToDailyCoaching() {
        this.dailyCoachingSub = true;
        this.save(flush: true);
    }

    def unsubscribeToDailyCoaching() {
        this.dailyCoachingSub = false;
        this.save(flush: true);
    }

    static Boolean isAlreadySubscribedToLanguage(Long msisdn, String language) {
        return SubscriptionProfile.findByMsisdnAndLanguage(msisdn, language)
    }

    def retrieveLanguages() {
        def selectedLang = [];
        def profiles = SubscriptionProfile.findByMsisdn(this.msisdn)

        for (profile in profiles) {
            selectedLang.add(profile.language)
        }

        return selectedLang
    }

    def retrieveLanguageAtIndex(int index) {
        def profiles = SubscriptionProfile.findByMsisdn(this.msisdn)

        int i = 0
        for (profile in profiles) {
            if (i == index) {
                return profile.language
            }
            i++
        }

        return ""
    }

    def removeLanguageAtIndex(int index) {
        def profiles = SubscriptionProfile.findByMsisdn(this.msisdn)

        int i = 0
        int id = null
        for (profile in profiles) {
            if (i == index) {
                id = profile.id
                break
            }
            i++
        }

        if (id) {
            SubscriptionProfile profile = SubscriptionProfile.get(id)
            profile.delete()
        }
    }
}
