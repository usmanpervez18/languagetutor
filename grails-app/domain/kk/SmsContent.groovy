package kk

import content.sms.ActualCodes

class SmsContent implements Comparable<SmsContent> {

    Date startDate
    Date endDate
    int noteId = 0
    Boolean learnLanguage
    Boolean dailyCoaching
    String smsText
    Boolean enabled = true
    Long smsFrom = ActualCodes.subFreeCode

    int compareTo(SmsContent t) {
        return noteId - t.noteId
    }

    @Override
    String toString() {
        return noteId + smsText
    }

    def beforeInsert() {
        if (startDate != null) {
            noteId = startDate.getTime().intdiv(1000L).intValue()
        }
    }

    def beforeUpdate() {
        if (startDate != null) {
            noteId = startDate.getTime().intdiv(1000L).intValue()
        }
    }
    static constraints = {
        startDate(unique: true, validator: { val, obj ->
            Date start = obj.properties['startDate']
            Date end = obj.properties['endDate']
            if (start == null || end == null) {
                return ["yp.cs.startDate.empty"]
            }
            if (start.compareTo(end) >= 0) {
                return ["yp.cs.startDate.gt.endDate"]
            }
        })
        smsText(blank: false)

    }

    public boolean shouldApply(Subscription subscription, SubscriptionProfile subscriptionProfile) {
        if (subscription.lastNotifyId >= noteId) return false

        if (subscriptionProfile.learnLanguageSub) {
            return true
        }

        return false
    }

    public boolean shouldApplyForLL(Subscription subscription, SubscriptionProfile subscriptionProfile) {
        if (subscription.lastNotifyId >= noteId) return false

        if (subscriptionProfile.learnLanguageSub) {
            return true
        }

        return false
    }

    public boolean shouldApplyForDC(Subscription subscription, SubscriptionProfile subscriptionProfile) {
        if (subscription.lastNotifyId >= noteId) return false

        if (subscriptionProfile.dailyCoachingSub) {
            return true
        }

        return false
    }

    static namedQueries = {
        notifiablePostings {
            le "startDate", new Date()
            ge "endDate", new Date()
            eq "enabled", true

        }
        notifiableLLMessages {
            le "startDate", new Date()
            ge "endDate", new Date()
            eq "learnLanguage", true
            eq "enabled", true
        }
        notifiableDCMessages {
            le "startDate", new Date()
            ge "endDate", new Date()
            eq "dailyCoaching", true
            eq "enabled", true
        }
        //Round the clock RE
        instantNotifiablePostings {
            le "startDate", new Date()
            ge "endDate", new Date()
            eq "enabled", true
            order "noteId", "asc"
        }

        onlyCount {
            projections {
                countDistinct('id')
            }
        }

    }
}
