package sms

import econ.api.IStatsCollector
import flow.sms.SmsSubscriberState
import grails.transaction.Transactional
import grails.util.Environment
import grails.util.Holders
import kk.Subscription
import kk.SubscriptionProfile
import kk.flow.I18nService
import org.springframework.beans.factory.annotation.Autowired
import sms.app.SubscriptionSetting

import java.text.SimpleDateFormat

@Transactional
class SubscriptionService {

    final String serviceUrl = Holders.config['zong.service.url']
    final static String vendorName = Holders.config['vendor.name']
    final static String serviceName = Holders.config['service.name']

    @Autowired
    IStatsCollector iStatsCollector

    I18nService i18nService

    String msg(String m, String lang, List arg = []) {
        //println(session.lang)
        return i18nService.message([code: m, locale: lang, attrs: arg])
    }

    Subscription getSubscription(Long msisdn) {
        return Subscription.get(msisdn)
    }

    SubscriptionSetting getSubscriptionSetting(Long msisdn) {
        return SubscriptionSetting.get(msisdn)
    }

    String getSubscriptionLang(Long msisdn) {
        SubscriptionSetting subscriptionSetting = SubscriptionSetting.get(msisdn)
        return subscriptionSetting?.lang
    }

    Subscription createSubscription(Long msisdn, subType = 'SMS') {
        Subscription subscription = new Subscription(id: msisdn, subType: subType)

        if (subscription.save(flush: true)) {

            if (Environment.current == Environment.PRODUCTION) {
                try {
                    String response = parseResponse(doPost("a", msisdn.toString()))
                    if (response?.trim()?.startsWith("1")) {
                        log.error("Zong Sub Intimation Success: [msisdn:${msisdn}] [response:${response}]")
                    } else {
                        log.error("Zong Sub Intimation Failed: [msisdn:${msisdn}] [response:${response}]")
                    }
                } catch (Exception ex) {
                    ex.printStackTrace()
                    log.error("Zong Sub Intimation Failed: [msisdn:${msisdn}] [exception:${ex.getMessage()}]")
                }
            } else {
                println "Intimating zong for SUB"
            }

            int NewSubscriptionStats = 1001
            int NewSubscriptionSMSStats = 1011
            int NewSubscriptionIVRStats = 1012
            int NewSubscriptionOBDStats = 1013
            int NewSubscriptionRecurringStats = 1015

            if (subType == 'IVR') {
                iStatsCollector.increment(NewSubscriptionIVRStats)
            } else if (subType == 'OBD') {
                iStatsCollector.increment(NewSubscriptionOBDStats)
            } else {
                iStatsCollector.increment(NewSubscriptionSMSStats)
            }

            SubscriptionSetting subscriptionSetting = getSubscriptionSetting(msisdn)

            if (subscriptionSetting) {
                subscription.lastNotifyId = subscriptionSetting.lastNotifyId
                subscription.save(flush: true)
                iStatsCollector.increment(NewSubscriptionRecurringStats)
            } else {
                new SubscriptionSetting(id: msisdn).save(flush: true)
                defaultSubscription(msisdn)
            }

            iStatsCollector.increment(NewSubscriptionStats)
            return subscription
        }
        return null
    }

    boolean defaultSubscription(Long msisdn) {
        new SubscriptionProfile(msisdn: msisdn, language: 'English', difficultyLevel: 'Basic', learnLanguageSub: false, dailyCoachingSub: false, dateCreated: new Date()).save()
        int CatSubStats = 10001
        iStatsCollector.increment(CatSubStats)
        return true
    }

    boolean unSubscription(Long msisdn) {
        Subscription subscription = Subscription.get(msisdn)
        SubscriptionSetting subscriptionSetting = getSubscriptionSetting(msisdn)
        subscriptionSetting.lastNotifyId = subscription.lastNotifyId
        if (subscription && subscriptionSetting.save(flush: true)) {

            subscription.delete()
            int UnsusbscriptionStats = 2001
            iStatsCollector.increment(UnsusbscriptionStats)

            SmsSubscriberState smsSubscriberState = SmsSubscriberState.findByMsisdn(msisdn)
            if (smsSubscriberState && smsSubscriberState != null) {
                smsSubscriberState.delete()
            }

            if (Environment.current == Environment.PRODUCTION) {
                try {
                    String response = parseResponse(doPost("d", msisdn.toString()))
                    if (response?.trim()?.startsWith("1")) {
                        log.error("Zong UnSub Intimation Success: [msisdn:${msisdn}] [response: ${response}]")
                    } else {
                        log.error("Zong UnSub Intimation Failed: [msisdn:${msisdn}] [response: ${response}]")
                    }
                } catch (Exception ex) {
                    ex.printStackTrace()
                    log.error("Zong UnSub Intimation Failed: [msisdn:${msisdn}] [exception: ${ex.getMessage()}]")
                }
            } else {
                println("Intimating zong for unsub")
            }

            return true
        }
        return false
    }

    public org.apache.http.client.HttpClient createHttpClient() {
        org.apache.http.client.HttpClient httpClient = new org.apache.http.impl.client.DefaultHttpClient();

        httpClient.params.setIntParameter("http.socket.timeout", 40000)
        httpClient.params.setIntParameter("http.connection.timeout", 40000)
        return httpClient
    }

    public String doPost(String status, String msisdn) {
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a")

        String content = """<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/">
   <soap:Header/>
   <soap:Body>
      <tem:AddData>
         <!--Optional:-->
         <tem:msisdn>92${msisdn}</tem:msisdn>
         <!--Optional:-->
         <tem:vendorname>${vendorName}</tem:vendorname>
         <!--Optional:-->
         <tem:status>${status}</tem:status>
         <!--Optional:-->
         <tem:servicename>${serviceName}</tem:servicename>
         <!--Optional:-->
         <tem:vendor_date>""" + formatter.format(new Date()) + """</tem:vendor_date>
      </tem:AddData>
   </soap:Body>
</soap:Envelope>""";

        org.apache.http.client.HttpClient httpClient = createHttpClient()
        org.apache.http.client.methods.HttpPost post = new org.apache.http.client.methods.HttpPost(serviceUrl)
        post.setEntity(new org.apache.http.entity.StringEntity(content, "application/soap+xml", "UTF-8"))
        org.apache.http.HttpResponse entity = httpClient.execute(post)

        if (entity == null || entity.entity == null) {
            return ""
        } else {
            return entity?.entity?.content?.text
        }

    }
}
