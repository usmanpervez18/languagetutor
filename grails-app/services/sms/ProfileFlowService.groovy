package sms

import econ.api.IFlowDelegate
import grails.transaction.Transactional
import kk.Subscription
import kk.flow.MainFlowService
import sms.flow.lib.FlowObject

@FlowObject
@Transactional
class ProfileFlowService {
    //static boolean transactional = false

    MainFlowService mainFlowService
    SubscriptionService subscriptionService

    final String mainLanguage = "language"
    final String invalidSelection = "invalid.selection"
    final int ProfStats = 100119

    final Closure Main = econ.api.helpers.Helpers.step { IFlowDelegate d ->
        if (sms.text?.trim()?.toLowerCase() != 'p' && mainFlowService.checkShortcut(d)) {
            return null
        }
        if (sms.text.trim().toLowerCase() == 'b') {
            forward mainFlowService.MainMenu
            return null
        }



        Subscription sub = subscriptionService.getSubscription(sms.from)

        if (d.isPostBack()) {


        } else {

            next Main
        }

    }

    String msg(String key, String lang, List arg = []) {
        return subscriptionService.msg(key, lang, arg)
    }
}
