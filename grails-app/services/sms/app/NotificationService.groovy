package sms.app

import groovy.transform.CompileStatic
import org.grails.events.ClosureEventConsumer
import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import reactor.bus.Event
import reactor.bus.EventBus
import reactor.bus.registry.Registration
import sms.timer.Intervals
import util.concurrent.SingleThreadBound

import java.util.concurrent.atomic.AtomicBoolean

@CompileStatic
class NotificationService implements DisposableBean, InitializingBean {
    static boolean transactional = false
    static boolean lazyInit = false

    private AtomicBoolean shutdown = new AtomicBoolean(false)
    //Allows only a single thread to enter a portion. If a new thread enters previous thread leaves.
    SingleThreadBound st = new SingleThreadBound()

    SubscriberNotificationsService subscriberNotificationsService
    @Autowired
    EventBus eventBus

    Registration saveReg

    @Override
    void afterPropertiesSet() throws Exception {
        ClosureEventConsumer<Long> cl = new ClosureEventConsumer<Long>({ Event<Long> l ->
            sendLearnLanguageNotification()
        })
        saveReg = eventBus.on(Intervals.everyHour, cl)
        log.info("Load: Learn Language Notification service loaded.")

        ClosureEventConsumer<Long> cl1 = new ClosureEventConsumer<Long>({ Event<Long> l ->
            sendDailyCoachingNotification()
        })
        saveReg = eventBus.on(Intervals.everyHour, cl1)
        log.info("Load: Daily Coaching Notification service loaded.")
    }

    @Override
    void destroy() throws Exception {
        shutdown.set(true)
        saveReg?.cancel()
        log.debug("Notification service: Destroyed.")
    }

    public void sendLearnLanguageNotification() {
        Calendar c = Calendar.instance
        int hour = c.get(Calendar.HOUR_OF_DAY)
        List day = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]

        st.run {
            if (day.contains(hour)) {
                if (subscriberNotificationsService.sendLearnLanguageNotifications() == 0) {
                    return
                }
            }

            !shutdown.get()
        }
    }

    public void sendDailyCoachingNotification() {
        Calendar c = Calendar.instance
        int hour = c.get(Calendar.HOUR_OF_DAY)
        List day = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]

        st.run {
            if (day.contains(hour)) {
                if (subscriberNotificationsService.sendDailyCoachingNotifications() == 0) {
                    return
                }
            }

            !shutdown.get()
        }
    }

}
