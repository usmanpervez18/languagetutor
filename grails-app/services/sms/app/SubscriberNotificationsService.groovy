package sms.app

import econ.api.IStatsCollector
import econ.api.SmsSender
import grails.transaction.Transactional
import kk.SmsContent
import kk.Subscription
import kk.SubscriptionProfile
import org.springframework.beans.factory.annotation.Autowired

@Transactional
class SubscriberNotificationsService {

    @Autowired
    SmsSender smsSender

    @Autowired
    IStatsCollector iStatsCollector

    public synchronized Set<SmsContent> messages = new TreeSet<SmsContent>();
    public synchronized long maxMessageId = 0L;

    final int PostingStatCode = 1000

    public Set<SmsContent> loadPostings() {
        Set<SmsContent> postings = new TreeSet<SmsContent>()
        SmsContent.notifiablePostings?.list()?.each { SmsContent smsPosting ->
            postings.add(smsPosting)
        }
        return postings
    }

    public Set<SmsContent> loadLearnLanguageMessages() {
        Set<SmsContent> postings = new TreeSet<SmsContent>()
        SmsContent.notifiableLLMessages?.list()?.each { SmsContent smsPosting ->
            postings.add(smsPosting)
        }
        return postings
    }

    public Set<SmsContent> loadDailyCoachingMessages() {
        Set<SmsContent> postings = new TreeSet<SmsContent>()
        SmsContent.notifiableDCMessages?.list()?.each { SmsContent smsPosting ->
            postings.add(smsPosting)
        }
        return postings
    }

    public List<Subscription> loadNotifiableLLSubscribers(int count = 25) {
        return Subscription.getNotifiableLLSubscribers(count).collect { it }
    }

    public List<Subscription> loadNotifiableDCSubscribers(int count = 25) {
        return Subscription.getNotifiableDCSubscribers(count).collect { it }
    }

    public int sendLearnLanguageNotifications(int max = 500) {

        messages = loadLearnLanguageMessages()

        maxMessageId = messages ? messages.max { it.noteId }.noteId : 0L

        int countHandled = 0
        List<Subscription> subs = loadNotifiableLLSubscribers(max)

        subs.each { Subscription sub ->
            boolean smsSent = false
            List subscriptionProfile = SubscriptionProfile.findAllByMsisdn(sub.id)

            subscriptionProfile.each { SubscriptionProfile sp ->

                if (!smsSent && sendLearnLanguageNotifications(sub, sp)) {
                    smsSent = true
                    countHandled++
                }
            }

            smsSent ? sub.updateNextLLNotificationTime(true) : sub.updateNextLLNotificationTime(false)
            sub.save(flush: true)
        }

        messages = new TreeSet<SmsContent>()
        log.debug("Notification service: CountHandeled = ${countHandled}")
        return countHandled
    }

    public int sendDailyCoachingNotifications(int max = 500) {

        messages = loadDailyCoachingMessages()

        maxMessageId = messages ? messages.max { it.noteId }.noteId : 0L

        int countHandled = 0
        List<Subscription> subs = loadNotifiableDCSubscribers(max)

        subs.each { Subscription sub ->
            boolean smsSent = false
            List subscriptionProfile = SubscriptionProfile.findAllByMsisdn(sub.id)

            subscriptionProfile.each { SubscriptionProfile sp ->

                if (!smsSent && sendDailyCoachingNotifications(sub, sp)) {
                    smsSent = true
                    countHandled++
                }
            }

            smsSent ? sub.updateNextDCNotificationTime(true) : sub.updateNextDCNotificationTime(false)
            sub.save(flush: true)
        }

        messages = new TreeSet<SmsContent>()
        log.debug("Notification service: CountHandeled = ${countHandled}")
        return countHandled
    }

    public boolean sendNotifications(Subscription subscription, SubscriptionProfile subscriptionProfile) {
        SmsContent post = messages.find { SmsContent posting ->
            return posting.shouldApply(subscription, subscriptionProfile)
        }

        if (!post) {
            return false
        }

        subscription.lastNotifyId = post.noteId
        sendMessage(subscription, post, subscriptionProfile)
    }

    public boolean sendLearnLanguageNotifications(Subscription subscription, SubscriptionProfile subscriptionProfile) {
        SmsContent post = messages.find { SmsContent posting ->
            return posting.shouldApplyForLL(subscription, subscriptionProfile)
        }

        if (!post) {
            return false
        }

        subscription.lastNotifyId = post.noteId
        sendMessage(subscription, post, subscriptionProfile)
    }

    public boolean sendDailyCoachingNotifications(Subscription subscription, SubscriptionProfile subscriptionProfile) {
        SmsContent post = messages.find { SmsContent posting ->
            return posting.shouldApplyForDC(subscription, subscriptionProfile)
        }

        if (!post) {
            return false
        }

        subscription.lastNotifyId = post.noteId
        sendMessage(subscription, post, subscriptionProfile)
    }

    public boolean sendMessage(Subscription subscription, SmsContent posting, SubscriptionProfile subscriptionProfile) {
        iStatsCollector.increment(PostingStatCode)
        String smsText = posting.smsText
        return smsSender.sendSMS(posting.smsFrom, subscription.id, smsText)
    }

    def serviceMethod() {

    }
}
