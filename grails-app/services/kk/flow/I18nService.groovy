package kk.flow

import grails.transaction.Transactional
import groovy.transform.CompileStatic
import org.springframework.context.MessageSource
import org.springframework.web.servlet.i18n.SessionLocaleResolver

@Transactional
class I18nService {

    SessionLocaleResolver localeResolver
    MessageSource messageSource

    /**
     *
     * @param msgKey
     * @param defaultMessage default message to use if none is defined in the message source
     * @param objs objects for use in the message
     * @return
     */
    String msg(String msgKey,String locale, String defaultMessage = null, List objs = null) {
        //println("Locale: ${locale}")
        Locale loc = locale == '' ? new Locale('en_r') : new Locale(locale)
        def msg = messageSource.getMessage(msgKey,objs?.toArray(),loc)

        if (msg == null || msg == defaultMessage) {
            log.warn("No i18n messages specified for msgKey: ${msgKey}")
            msg = defaultMessage
        }

        return msg
    }

    /**
     * @param args
     * @return
     */
    String message(Map args) {
        return msg(args.code, args.locale, args.default, args.attrs)
    }
}
