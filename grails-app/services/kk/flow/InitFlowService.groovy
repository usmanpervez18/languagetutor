package kk.flow

import econ.api.MessageConsumer
import econ.api.SmsSender
import grails.util.Environment
import org.apache.commons.collections.functors.ExceptionClosure
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import sms.flow.FlowService
import sms.flow.lib.errors.ClosureNotFound

import static content.sms.ActualCodes.*

class InitFlowService implements InitializingBean,MessageConsumer{
    static boolean transactional = false
    static boolean lazyInit = false
    /*@Autowired
    SmsSender smsSender*/
    
    final int defaultTo = (int)subFreeCode
    final boolean PROD = Environment.current == Environment.PRODUCTION

    FlowService flowService

    @Override
    boolean received(Long from, Long to, String text) {
        try{
            log.info("SMS Received: $to $from $text")

            if (to != defaultTo) return true

            return flowService.execute( from , to , text , to.toInteger() )
        }catch(Exception e){
            log.error("Closure not found ${e}")
            return true
        }
    }

    @Override
    void afterPropertiesSet() throws Exception {
        flowService.addDefaultFlow( defaultTo , "mainFlowService.entryPoint")
        if( ! PROD ){
            flowService.defaultValidity = 120 // 2mins.
        } else{
            flowService.defaultValidity = 7200 // 2hours.
        }
    }

}
