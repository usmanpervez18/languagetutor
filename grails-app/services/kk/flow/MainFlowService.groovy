package kk.flow

import econ.api.IFlowDelegate
import grails.transaction.Transactional
import kk.Lecture
import kk.Subscription
import kk.SubscriptionProfile
import kk.TestQuestion
import org.springframework.beans.factory.InitializingBean
import sms.SubscriptionService
import sms.app.SubscriptionSetting
import sms.flow.lib.FlowObject

import static econ.api.helpers.Helpers.step

@FlowObject
@Transactional
class MainFlowService implements InitializingBean {

    //region Custom Variables
    SubscriptionService subscriptionService

    def selectedLanguage = 'English'
    def pageNumber = 1
    def questionNumber = 1
    def message = ""

    Map<String, Closure> flowShortcuts = [:]

    final String welcomeReSub = "welcome.resub"
    final String welcome = "welcome"
    final String mm = "mm"
    final String mainLanguage2 = "l.language"
    final String welcomeUser = "welcome.user"


    final String help = "help"

    final String unsubConfirm = ""
    final String INVALID_SUB_MSG = "invalid.subscriber"
    final String unsubscribed = "Unsubscribed"
    final String languageSuccess = "language.success"

    final int NewSubscriptionStats = 1001
    final int UnsusbscriptionStats = 2001

    final String UNSUBSCRIBE = 'unsub'
    final def VALID_SUBS_KEYS = ['sub', 'osub', 'smsub', 'y']
    final String REQUEST_ERROR = "There was an error processing your request. Please try again later."
    final String DEFAULT_LANG = 'en'
    final static def languages = ['English', 'French', 'Arabic', 'Spanish']
    //endregion

    //region Overridden Methods
    @Override
    void afterPropertiesSet() throws Exception {
        /*
        * Shortcuts**/
        flowShortcuts = [
                'm'    : "mainFlowService.mainMenu",
                'll'   : "mainFlowService.chooseLanguageForLL",
                'dc'   : "mainFlowService.chooseLanguageForDC",
                'al'   : "mainFlowService.chooseLanguageForAL",
                'st'   : "mainFlowService.chooseLanguageForST",
                'h'    : "mainFlowService.helpMenu",
                'hep'  : "mainFlowService.helpMenu",
                'help' : "mainFlowService.helpMenu",
                'hlp'  : "mainFlowService.helpMenu",
                'hp'   : "mainFlowService.helpMenu",
                'he'   : "mainFlowService.helpMenu",
                'hl'   : "mainFlowService.helpMenu",
                'hel'  : "mainFlowService.helpMenu",
                'vp'   : "mainFlowService.updateProfile",
                'p'    : "mainFlowService.updateProfile",
                'unsub': "mainFlowService.unsub",
                'sub'  : "mainFlowService.entryPoint"
        ]

        log.info("Inited mainFlowService")
    }
    //endregion

    final Closure unsub = step { IFlowDelegate d ->

        Subscription sub = subscriptionService.getSubscription(sms.from)

        if (sub) {

            if (sms.text != "unsub" && checkShortcut(delegate as IFlowDelegate)) {
                return null
            }

            def inputKey = d.sms.text?.toLowerCase()

            switch (inputKey) {
                case '0':
                    subscriptionService.unSubscription(sms.from)
                    send msg("unsub.success", session.lang)
                    endSession()
                    return null
                case ['m', 'M']:
                    send msg("mm", session.lang)
                    next GoToMenu
                    return null
                default:
                    send msg("unsub.invalid", session.lang)
                    next unsub
                    return null
            }
        } else {
            send msg(INVALID_SUB_MSG, session.lang)
            endSession()
        }
    }

    final Closure EntryPoint = econ.api.helpers.Helpers.step { IFlowDelegate d ->

        Subscription sub = subscriptionService.getSubscription(sms.from)

        if (sub == null) {

            def inputKey = d.sms.text?.toLowerCase()

            if (inputKey != UNSUBSCRIBE) {

                if (VALID_SUBS_KEYS.contains(inputKey)) {

                    String subType = inputKey == 'osub' ? 'OBD' : 'SMS'

                    boolean resub = false
                    if (SubscriptionSetting.get(sms.from)) {
                        resub = true
                    }

                    sub = subscriptionService.createSubscription(sms.from, subType)

                    if (!sub) {
                        send REQUEST_ERROR
                        log.error("Count not save sub. ${sub.errors.allErrors.join("\n")}")
                        endSession()
                    }

                    session.lang = subscriptionService.getSubscriptionLang(sms.from)

                    /*if (resub) {
                        send msg('welcome.resub', session.lang) + msg('diff.level.options', session.lang)
                    } else {
                        send msg("l.language", session.lang)
                    }*/

                    send msg("l.language", session.lang)
                    next SelectLanguage
                    return null

                } else {
                    send msg(INVALID_SUB_MSG, DEFAULT_LANG)
                    next EntryPoint;
                }
            } else {
                send msg(INVALID_SUB_MSG, DEFAULT_LANG)
                next EntryPoint;
            }
        } else {

            session.lang = subscriptionService.getSubscriptionLang(sms.from)

            String smsText = sms.text?.trim()?.toLowerCase()

            if (!["m", "sub"].contains(smsText) && checkShortcut(delegate as IFlowDelegate)) {
                return null
            }

            if (smsText == "sub") {
                send msg("already.sub", session.lang)
                next GoToMenu
                return null
            }

            if (checkShortcut(d)) {
                return null
            }

            forward InvalidResponse
        }

    }

    final Closure SelectLanguage = step { IFlowDelegate d ->

        switch (sms.asInt()) {
            case 1..4:

                if (!SubscriptionProfile.isAlreadySubscribedToLanguage(sms.from, languages[sms.asInt() - 1])) {
                    new SubscriptionProfile(msisdn: sms.from, language: languages[sms.asInt() - 1], difficultyLevel: 'Basic', learnLanguageSub: false, dailyCoachingSub: false, dateCreated: new Date()).save()
                }

                flash = languages[sms.asInt() - 1]
                send msg("select.diff.level", session.lang, [languages[sms.asInt() - 1]])
                next SelectDiffLevel
                return null
            default:
                send msg("invalid.lang.selected", session.lang)
                next SelectLanguage
                return null
        }
    }

    final Closure SelectDiffLevel = step { IFlowDelegate d ->

        def levels = ['Basic', 'Intermediate', 'Advanced']

        def inputKey = d.sms.text?.toLowerCase()

        switch (inputKey) {

            case ['1', '2', '3']:

                SubscriptionProfile profile = SubscriptionProfile.findByMsisdnAndLanguage(sms.from, flash)

                profile.difficultyLevel = levels[sms.asInt() - 1]
                profile.save();

                flash = profile.language
                send msg("setup.successful", session.lang, [profile.language, levels[sms.asInt() - 1]])
                next SetupConfirmation
                return null

            case ['b', 'B']:

                send msg("l.language", session.lang)
                next SelectLanguage
                return null

            default:

                send msg("invalid.diff.selected", session.lang)
                next SelectDiffLevel
                return null
        }

    }

    final Closure MainMenu = step { IFlowDelegate d ->

        def inputKey = d.sms.text?.toLowerCase()
        message = null

        if (checkShortcut(d)) {
            return null
        }

        switch (inputKey) {

            case '1':

                processLearnLanguageRequest(d)
                return null

            case '2':

                processDailyCoachingRequest(d)
                return null

            case '3':

                processAllLecturesRequest(d)
                return null

            case '4':

                processSkillTestRequest(d)
                return null

            case '5':

                processViewProfileRequest(d)
                return null

            default:
                send msg("invalid.selection", session.lang) + " " + msg('mm', session.lang)
                next MainMenu
                return null
        }
    }

    final Closure VerifyAnswer = step { IFlowDelegate d ->

        def inputKey = d.sms.text?.toLowerCase()

        switch (inputKey) {

            case ['m', 'M']:
                send msg("mm", session.lang)
                next MainMenu
                return null

            default:

                TestQuestion previousQ = TestQuestion.getTestQuestion(selectedLanguage, questionNumber)

                questionNumber = TestQuestion.countByLanguage(selectedLanguage) < (questionNumber + 1) ? 1 : questionNumber + 1

                TestQuestion nextQ = TestQuestion.getTestQuestion(selectedLanguage, questionNumber)
                String nextQFormatted = TestQuestion.getFormattedQuestion(nextQ)

                if (previousQ.correctOption == sms.asInt()) {
                    send msg("correct.answer", session.lang) + nextQFormatted + "\n\n" + msg("goto.menu", session.lang)
                } else {
                    send msg("incorrect.answer", session.lang, [previousQ.getCorrectOptionValue()]) + nextQFormatted + "\n\n" + msg("goto.menu", session.lang)
                }

                next VerifyAnswer
                return null
        }
    }

    final Closure UnsubCategory = step { IFlowDelegate d ->

        SubscriptionProfile profile = null
        if (SubscriptionProfile.subscribedToMultipleLanguages()) {
            profile = SubscriptionProfile.findByMsisdnAndLanguage(sms.from, selectedLanguage)
        } else {
            profile = SubscriptionProfile.findByMsisdn(sms.from)
        }

        def inputKey = d.sms.text?.toLowerCase()

        switch (inputKey) {
            case '1':

                profile.unsubscribeToLearnLanguage()

                if (profile.dailyCoachingSub) {
                    message = msg("unsub.cat.success", session.lang, ["To Learn a Language (LL)", "\n2. Daily Coaching (DC)"])
                    send message
                } else {
                    message = msg("unsub.cat.all", session.lang, ["To Learn a Language (LL)"])
                    send message
                }

                next UnsubCategory
                return null

            case '2':

                profile.unsubscribeToDailyCoaching()

                if (profile.learnLanguageSub) {
                    message = msg("unsub.cat.success", session.lang, ["Daily Coaching (DC)", "\n1. To Learn a Language (LL)"])
                    send message
                } else {
                    message = msg("unsub.cat.all", session.lang, ["Daily Coaching (DC)"])
                    send message
                }

                next UnsubCategory
                return null

            case '0':

                send msg("unsub.confirm", session.lang)
                next unsub
                return null

            case ['m', 'M']:
                send msg("mm", session.lang)
                next MainMenu
                return null

            default:

                if (message) {
                    send msg("invalid.unsub", session.lang, [message])
                    next UnsubCategory
                    return null
                } else {
                    send msg("invalid.unsub", session.lang) + msg("goto.menu", session.lang)
                    next GoToMenu
                    return null
                }

        }
    }

    final Closure UnsubLanguage = step { IFlowDelegate d ->

        SubscriptionProfile profile = SubscriptionProfile.findByMsisdn(sms.from);

        def inputKey = d.sms.text?.toLowerCase()

        if (['m', 'M'].contains(inputKey)) {
            send msg("mm", session.lang)
            next MainMenu
            return null
        }

        switch (sms.asInt()) {
            case 1..SubscriptionProfile.countByMsisdn(sms.from):
                selectedLanguage = languages[sms.asInt() - 1]
                send msg("unsub.options", session.lang)
                next UnsubOptions
                break
            case 0:
                break;
            default:
                send message
                next UnsubLanguage
                return null
        }
    }

    final Closure UnsubOptions = step { IFlowDelegate d ->

        SubscriptionProfile profile = SubscriptionProfile.findByMsisdnAndLanguage(sms.from, selectedLanguage)

        switch (sms.asInt()) {

            case 1:

                profile.delete()

                send msg("unsub.lang.success", session.lang, [selectedLanguage])
                next GoToMenu
                return null

            case 2:

                String info = ""
                if (profile.learnLanguageSub) {
                    info = "To unsubscribe please reply with\n1. To Learn a Language"
                }

                if (profile.dailyCoachingSub) {
                    info += "\n2. Daily Coaching"
                }

                message = msg("category.info", session.lang, [info])
                send message
                next UnsubCategory
                return null

            default:
                send msg("invalid.unsub.options", session.lang)
                next UnsubOptions
                return null
        }

    }

    final Closure InvalidResponse = econ.api.helpers.Helpers.step {
        String text = msg('invalid.response', session.lang)

        send text
        endSession()
    }

    final Closure HelpMenu = econ.api.helpers.Helpers.step {
        String text = msg(help, session.lang)

        send text
        endSession()
    }

    final Closure AlreadySub = econ.api.helpers.Helpers.step { IFlowDelegate d ->
        flash = "already.sub"
        forward MainMenu
        return null
    }

    final Closure GoToMenu = econ.api.helpers.Helpers.step { IFlowDelegate d ->

        def inputKey = d.sms.text?.toLowerCase()
        message = null
        selectedLanguage = 'English'
        pageNumber = 1
        questionNumber = 1

        switch (inputKey) {
            case ['m', 'M']:
                message = null
                send msg("mm", session.lang)
                next MainMenu
                return null
            case ['h', 'H']:
                message = null
                processHelpRequest(d)
                return null
            default:
                send msg("invalid.option.selected2", session.lang)
                next MainMenu
                return null
        }
    }

    final Closure PostDCSubscription = econ.api.helpers.Helpers.step { IFlowDelegate d ->

        def inputKey = d.sms.text?.toLowerCase()

        if (['m', 'M'].contains(inputKey)) {
            send msg("mm", session.lang)
            next MainMenu
            return null
        } else if (['p', 'P'].contains(inputKey)) {
            send msg("dc.multiple.sub", session.lang, ["Daily Coaching", SubscriptionProfile.getSubscribedLanguages(sms.from)])
            next ChooseLanguageForDC
            return null
        } else {
            send msg("invalid.option.selected2", session.lang)
            next MainMenu
            return null
        }
    }

    final Closure ChooseLanguageForLL = step { IFlowDelegate d ->

        def profiles = SubscriptionProfile.findAllByMsisdn(sms.from)

        switch (sms.asInt()) {

            case 1..profiles.size():

                def profile = profiles[sms.asInt() - 1]
                profile.subscribeToLearnLanguage()

                send msg("ll.sub", session.lang)
                next GoToMenu
                return null

            default:

                send msg("ll.sub.invalid", session.lang, [SubscriptionProfile.getSubscribedLanguages(sms.from)])
                next ChooseLanguageForLL
                return null
        }
    }

    final Closure ChooseLanguageForDC = step { IFlowDelegate d ->

        def profiles = SubscriptionProfile.findAllByMsisdn(sms.from)

        switch (sms.asInt()) {

            case 1..profiles.size():

                def profile = profiles[sms.asInt() - 1]
                profile.subscribeToDailyCoaching()

                send msg("dc.single.sub", session.lang, [profile.language])
                next PostDCSubscription
                return null

            default:

                send msg("dc.sub.invalid", session.lang, [SubscriptionProfile.getSubscribedLanguages(sms.from)])
                next ChooseLanguageForDC
                return null
        }
    }

    final Closure ChooseLanguageForAL = step { IFlowDelegate d ->

        switch (sms.asInt()) {

            case 1..SubscriptionProfile.countByMsisdn(sms.from):

                selectedLanguage = languages[sms.asInt() - 1]
                def lectures = Lecture.getLectures(selectedLanguage);

                if (lectures) {

                    pageNumber = 1
                    def lecturesStr = Lecture.getConcatenatedLectures(lectures, pageNumber)

                    send msg("show.lectures", session.lang, [lecturesStr])
                    next SelectLecture
                }

                return null

            default:

                send msg("invalid.select.al.lang", session.lang, [SubscriptionProfile.getSubscribedLanguages(sms.from)])
                next ChooseLanguageForAL
                return null
        }

    }

    final Closure ChooseLanguageForST = step { IFlowDelegate d ->

        switch (sms.asInt()) {

            case 1..SubscriptionProfile.countByMsisdn(sms.from):

                selectedLanguage = languages[sms.asInt() - 1]
                questionNumber = 1
                TestQuestion question = TestQuestion.getTestQuestion(selectedLanguage, questionNumber)

                if (question) {

                    String formattedQ = TestQuestion.getFormattedQuestion(question)

                    send formattedQ
                    next VerifyAnswer
                    return null

                } else {
                    send msg("no.questions", session.lang)
                    next GoToMenu
                    return null
                }

            default:

                send msg("invalid.select.st.lang", session.lang, [SubscriptionProfile.getSubscribedLanguages(sms.from)])
                next ChooseLanguageForST
                return null
        }

    }

    final Closure UpdateProfile = step { IFlowDelegate d ->

        def inputKey = d.sms.text?.toLowerCase()

        switch (inputKey) {

            case ['p', 'P']:

                break

            case ['r', 'R']:

                message = msg("unsub.language", session.lang, [SubscriptionProfile.getSubscribedLanguages(sms.from)])
                send message
                next UnsubLanguage
                return null

            default:
                send message
                next UpdateProfile
                return null
        }
    }

    final Closure SelectLecture = step { IFlowDelegate d ->

        def inputKey = d.sms.text?.toLowerCase()

        List<String> lectureIds = Lecture.getLectureIdsByLanguage(selectedLanguage)

        switch (inputKey) {

            case lectureIds:

                break

            case ['l', 'L']:

                def lectures = Lecture.getLectures(selectedLanguage, 5, (5 * pageNumber))

                if (lectures) {

                    send msg("show.lectures", session.lang, [Lecture.getConcatenatedLectures(lectures, ++pageNumber)])
                    next SelectLecture
                } else {

                    lectures = Lecture.getLectures(selectedLanguage)

                    if (lectures) {
                        pageNumber = 1
                        send msg("no.lectures", session.lang) + msg("show.lectures", session.lang, [Lecture.getConcatenatedLectures(lectures, pageNumber)])
                        next SelectLecture
                    }
                }

                return null


            case ['p', 'P']:

                def lectures = Lecture.getLectures(selectedLanguage, 5, (pageNumber - 2) * 5)

                if (lectures) {
                    send msg("show.lectures", session.lang, [Lecture.getConcatenatedLectures(lectures, --pageNumber)])
                    next SelectLecture
                }

                return null

            case ['m', 'M']:

                send msg("mm", session.lang)
                next MainMenu
                return null

            case ['b', 'B']:
                send msg("select.al.lang", session.lang, [SubscriptionProfile.getSubscribedLanguages(sms.from)])
                next ChooseLanguageForAL
                return null

            default:

                def lectures = Lecture.getLectures(selectedLanguage, 5, (5 * (pageNumber - 1)))

                if (lectures) {

                    send msg("invalid.select.lec", session.lang, [Lecture.getConcatenatedLectures(lectures, pageNumber)])
                    next SelectLecture
                    return null

                } else {

                    send msg("no.lectures.menu", session.lang)
                    next MainMenu
                    return null
                }
        }

    }

    final Closure SetupConfirmation = step { IFlowDelegate d ->

        def inputKey = d.sms.text?.toLowerCase()

        if (['b', 'B'].contains(inputKey)) {

            send msg("mm", session.lang)
            next MainMenu
            return null

        } else {

            send msg("invalid.option.selected1", session.lang)
            next MainMenu
            return null
        }
    }

    String msg(String key, String lang, List arg = []) {
        return subscriptionService.msg(key, lang, arg)
    }

    boolean checkShortcut(IFlowDelegate d) {

        def inputKey = d.sms.text?.trim()?.toLowerCase()

        switch (inputKey) {
            case 'm':

                message = null
                d.send msg("mm", d.session.lang)
                d.next MainMenu
                return true

            case 'll':

                processLearnLanguageRequest(d)
                return true

            case 'dc':

                processDailyCoachingRequest(d)
                return true

            case 'al':

                processAllLecturesRequest(d)
                return true

            case 'st':

                processSkillTestRequest(d)
                return true

            case ['h', 'hep', 'help', 'hlp', 'hp', 'he', 'hl', 'hel']:

                processHelpRequest(d)
                return true

            case ['p', 'vp']:

                processViewProfileRequest(d)
                return true

            case 'unsub':

                d.send msg("unsub.confirm", d.session.lang)
                d.next unsub
                return null
        }

        return false
    }

    final Closure processLearnLanguageRequest = step { IFlowDelegate d ->

        if (SubscriptionProfile.subscribedToMultipleLanguages(d.sms.from)) {

            d.send msg("ll.multiple.sub", d.session.lang, [SubscriptionProfile.getSubscribedLanguages(d.sms.from)])
            d.next ChooseLanguageForLL
            return null

        } else {

            SubscriptionProfile profile = SubscriptionProfile.findByMsisdn(d.sms.from)
            profile.subscribeToLearnLanguage()

            d.send msg("ll.single.sub", d.session.lang)
            d.next GoToMenu
            return null
        }
    }

    final Closure processDailyCoachingRequest = step { IFlowDelegate d ->

        if (SubscriptionProfile.subscribedToMultipleLanguages(d.sms.from)) {

            d.send msg("dc.multiple.sub", d.session.lang, ["Daily Coaching", SubscriptionProfile.getSubscribedLanguages(d.sms.from)])
            d.next ChooseLanguageForDC
            return null

        } else {

            SubscriptionProfile profile = SubscriptionProfile.findByMsisdn(d.sms.from)
            profile.subscribeToDailyCoaching()

            d.send msg("dc.single.sub", d.session.lang, [profile.language])
            d.next PostDCSubscription
            return null
        }

    }

    final Closure processAllLecturesRequest = step { IFlowDelegate d ->

        if (SubscriptionProfile.subscribedToMultipleLanguages(d.sms.from)) {

            d.send msg("select.al.lang", d.session.lang, [SubscriptionProfile.getSubscribedLanguages(d.sms.from)])
            d.next ChooseLanguageForAL
            return null

        } else {

            SubscriptionProfile profile = SubscriptionProfile.findByMsisdn(d.sms.from)
            def lectures = Lecture.getLectures(profile.language)

            if (lectures) {

                pageNumber = 1
                d.send msg("show.lectures", d.session.lang, [Lecture.getConcatenatedLectures(lectures, pageNumber)])
                d.next SelectLecture
                return null

            } else {
                d.send msg("no.lectures.menu", d.session.lang)
                d.next GoToMenu
                return null
            }
        }
    }

    final Closure processSkillTestRequest = step { IFlowDelegate d ->

        if (SubscriptionProfile.subscribedToMultipleLanguages(d.sms.from)) {

            d.send msg("select.st.lang", d.session.lang, [SubscriptionProfile.getSubscribedLanguages(d.sms.from)])
            d.next ChooseLanguageForST
            return null

        } else {

            questionNumber = 1
            TestQuestion question = TestQuestion.getTestQuestion(selectedLanguage, questionNumber)

            if (question) {

                String formattedQ = TestQuestion.getFormattedQuestion(question)

                d.send formattedQ
                d.next VerifyAnswer
                return null

            } else {
                d.send msg("no.questions", d.session.lang)
                d.next GoToMenu
                return null
            }
        }
    }

    final Closure processViewProfileRequest = step { IFlowDelegate d ->

        if (SubscriptionProfile.subscribedToMultipleLanguages(d.sms.from)) {

            String info = "";
            def profiles = SubscriptionProfile.findAllByMsisdn(d.sms.from)

            for (profile in profiles) {
                info += "\n${profile.language} Language, ${profile.difficultyLevel} Level"

                if (profile.learnLanguageSub && profile.dailyCoachingSub) {
                    info += " in LL and DC Category"
                } else if (profile.learnLanguageSub) {
                    info += " in LL Category"
                } else if (profile.dailyCoachingSub) {
                    info += " in DC Category"
                }
            }

            message = msg("profile.info", d.session.lang, [info])
            d.send message
            d.next UpdateProfile
            return null

        } else {

            SubscriptionProfile profile = SubscriptionProfile.findByMsisdn(d.sms.from)

            String info = ""
            if (profile.learnLanguageSub && profile.dailyCoachingSub) {
                info = "To unsubscribe please reply with\n1. To Learn a Language\n2. Daily Coaching"
            } else if (profile.learnLanguageSub) {
                info = "To unsubscribe please reply with\n1. To Learn a Language"
            } else if (profile.dailyCoachingSub) {
                info = "To unsubscribe please reply with\n2. Daily Coaching"
            } else {
                message = msg("no.category.info", d.session.lang)
                d.send message
                d.next UnsubLanguage
                return null
            }

            message = msg("category.info", d.session.lang, [info])
            d.send message
            d.next UnsubCategory
            return null
        }
    }

    final Closure processHelpRequest = step { IFlowDelegate d ->
        d.send msg("help", d.session.lang)
        d.next MainMenu
        return null
    }
}
