import kk.Lecture
import kk.TestQuestion

class BootStrap {

    def init = { servletContext ->

        def languages = ['English', 'French', 'Arabic', 'Spanish']

        if (!Lecture.findAll()) {
            for (language in languages) {
                for (int i = 1; i <= 12; i++) {
                    new Lecture(title: "${language}", language: language, lectureNumber: i).save(flush: true)
                }
            }
        }

        if (!TestQuestion.findAll()) {
            String q = "Dear Subscriber, reply with the correct meaning of the word "

            for (language in languages) {
                new TestQuestion(language: language, question: q + "AAA?", option1: "AAA", option2: "BBB", option3: "CCC", option4: "DDD", correctOption: 1, questionNumber: 1).save()
                new TestQuestion(language: language, question: q + "BBB?", option1: "AAA", option2: "BBB", option3: "CCC", option4: "DDD", correctOption: 2, questionNumber: 2).save()
                new TestQuestion(language: language, question: q + "CCC?", option1: "AAA", option2: "BBB", option3: "CCC", option4: "DDD", correctOption: 3, questionNumber: 3).save()
                new TestQuestion(language: language, question: q + "DDD?", option1: "AAA", option2: "BBB", option3: "CCC", option4: "DDD", correctOption: 4, questionNumber: 4).save()
            }
        }
    }
    def destroy = {
    }
}
