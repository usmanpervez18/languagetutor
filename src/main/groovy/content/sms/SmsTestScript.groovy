package content.sms

import static java.net.URLEncoder.encode

/**
 * Created by khurramijaz on 13/05/2015.
 */
class SmsTestScript {

    def enc(String u){
        return encode(u,"UTF-8");
    }


    Long from = 3125666666L
    Long to = 7061L
    String text = "1"
    boolean del = false

    def doRequest(){
        String url = "http://localhost:8080/smsTest?from=${from}&to=${to}&text=${enc(text)}&flowKey=${to}&del=${del}"
        println(url)
        println(url.toURL().text)
    }


}
