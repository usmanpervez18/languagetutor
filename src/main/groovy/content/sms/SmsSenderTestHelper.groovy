package content.sms

/**
 * Created by IntelliJ IDEA.
 * User: khurram
 * Date: 26/3/12
 * Time: 1:52 PM
 * To change this template use File | Settings | File Templates.
 */
/**
 * Using duck typing.
 */
class SmsSenderTestHelper {
    String from
    String to
    String text


    public boolean sendSms(Long _from, Long _to, String _text){
        from = _from.toString()
        to = _to.toString()
        text = _text
        return true
    }
    public boolean sendSms(String _from, Long _to, String _text){
        from = _from
        to = _to.toString()
        text = _text
        return true
    }

}
