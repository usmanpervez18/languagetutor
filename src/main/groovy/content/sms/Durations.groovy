package content.sms

import java.text.SimpleDateFormat

/**
 * Created by IntelliJ IDEA.
 * User: khurram
 * Date: 19/3/12
 * Time: 1:07 PM
 * To change this template use File | Settings | File Templates.
 */
class Durations {

    public static final int SMSGAP= 24 * 3600 //24 hours
    public static final int NEWSUBDELAY = 900 //15 min
    public static final int NOOPDELAY = 3600 //1 hour
    public static final int NOOPDELAY1 = 60*5 //5 min
    public static final int NOOPDELAY2 = 60*10 //10 min
    public static final int NOOPDELAY3 = 60*30 //30 min
    public static final int NOOPDELAY4 = 3600 //1 hour
    public static final int BILLVALID = 7 * 24 * 3600 //7 day
    public static final int FOURWEEKS = 28 * 24 * 3600 //28 day


	public static int getCurrentSecond(){
        System.currentTimeMillis().intdiv( 1000L ).intValue()
    }

	public static int getLastValidBillTime(){
		Calendar c = Calendar.instance
		c.add( Calendar.DAY_OF_MONTH , -1 )
        return c.timeInMillis.intdiv( 1000L ).intValue()
	}

	public static int getBillDueTime(){
		Calendar c = Calendar.instance
        c.add( Calendar.DAY_OF_MONTH, +6 )
        return c.timeInMillis.intdiv( 1000L ).intValue()
	}
	public static int getToday(){
		Calendar c = Calendar.instance
		c.set( Calendar.HOUR_OF_DAY , 0 )
        c.set( Calendar.MINUTE, 0 )
        c.set( Calendar.SECOND, 0 )
        return c.timeInMillis.intdiv( 1000L ).intValue()
	}

    public static int getLastTwoWeeks(){
        Calendar c = Calendar.instance
        c.add( Calendar.DAY_OF_MONTH, -14 )
        c.set( Calendar.HOUR_OF_DAY , 0 )
        c.set( Calendar.MINUTE, 0 )
        c.set( Calendar.SECOND, 0 )
        return c.timeInMillis.intdiv( 1000L ).intValue()
    }

    public static int getServiceValidity(){
        Calendar c = Calendar.instance
        c.add( Calendar.DAY_OF_MONTH, +6 )
        c.set( Calendar.HOUR_OF_DAY , 23 )
        c.set( Calendar.MINUTE, 59 )
        c.set( Calendar.SECOND, 59 )
        return c.timeInMillis.intdiv( 1000L ).intValue()
    }

    public static int getDaysSeconds( int days){
        return new Date().time.intdiv(1000).intValue() - (60*60*24*days)
    }

    public static String convertSecondsInDate(long seconds) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        long milliseconds = seconds * 1000
        return formatter.format(new Date(milliseconds))
    }

    public static int getYesterday(){
        Calendar c = Calendar.instance
        c.add( Calendar.DATE, -1 )
        c.set( Calendar.HOUR_OF_DAY , 0 )
        c.set( Calendar.MINUTE, 0 )
        c.set( Calendar.SECOND, 0 )
        return c.timeInMillis.intdiv( 1000L ).intValue()
    }

    public static int getTomorrow(){
        Calendar c = Calendar.instance
        c.add( Calendar.DATE, +1 )
        c.set( Calendar.HOUR_OF_DAY , 0 )
        c.set( Calendar.MINUTE, 0 )
        c.set( Calendar.SECOND, 0 )
        return c.timeInMillis.intdiv( 1000L ).intValue()
    }

    public static int getYesterday2(){
        Calendar c = Calendar.instance
        c.add( Calendar.DATE, -1 )
        c.set( Calendar.HOUR_OF_DAY , 23 )
        c.set( Calendar.MINUTE, 59 )
        c.set( Calendar.SECOND, 59 )
        return c.timeInMillis.intdiv( 1000L ).intValue()
    }
}
