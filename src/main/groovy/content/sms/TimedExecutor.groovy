package content.sms

/**
 * Created by IntelliJ IDEA.
 * User: khurram
 * Date: 8/3/12
 * Time: 6:10 PM
 * To change this template use File | Settings | File Templates.
 */
class TimedExecutor implements Runnable{
    long maxTime = 10000L
    int maxCount = 100

    int count = 0
    long start = System.currentTimeMillis()
    Closure closure = {}
    public TimedExecutor( long maxTime , int maxCount, Closure c){
        this.maxTime = maxTime
        this.maxCount = maxCount
        closure = c
        c.delegate = this
        run()
    }
    public TimedExecutor(){



    }
    boolean _stop = false
    public void stop(){
        _stop = true
    }
    public boolean isTimeUp(){
        return ( System.currentTimeMillis() - start ) > maxTime
    }
    public boolean isCountUp(){
        return count >= maxCount
    }
    public void incCount(){
        count++
    }
    List collectedItems = []
    public void runClosure(){
        if( closure.maximumNumberOfParameters == 0 ){
            closure.call()
        }
        else{
            collectedItems << closure( count )
        }
    }
    public void run(){
        while( ! (isTimeUp() || isCountUp() || _stop ) ){
            runClosure()
            incCount()
        }
    }
    
    public static List execute(long maxTime, int maxCount , Closure c){
        new TimedExecutor( maxTime , maxCount , c ).collectedItems
    }
}
