package content.sms

/**
 * Created by IntelliJ IDEA.
 * User: khurram
 * Date: 13/3/12
 * Time: 12:32 PM
 * To change this template use File | Settings | File Templates.
 */
class LogicalCodes {
    static final long Purchase5Start = 1001001;
    static final long Purchase10Start = 1001002;

    static final long PurchaseBG = 1001003;
    static final long PurchaseVIDEO = 1001004;
    static final long PurchaseRT = 1001005;

    final static long FollowUnsub = 1001006;

    final static long SentVoteCode = 10027051
    final static long SentPredictCode = 10027052
    final static long SentFollowCode = 10027053
    final static long SentTriviaCode = 10027054

    final static long SentQuestion = 10027055
    final static long SentQuestionAgain = 10027056
    final static long ReceivedAnswer = 10027057




    final static long TriviaSub = 10027058
    final static long TriviaUnsub = 10027059
    final static long TriviaAnswer = 10027060
    final static long TriviaHelp = 10027061

    final static long SentFollowSms = 10027062
    public static String translateCode(int code){
        switch( code ){
            case Purchase5Start:
                return "Purchase 5 Attempt"
            case Purchase10Start:
                return  "Purchase 10 Attempt"
            case PurchaseBG:
                return  "Purchase BG"
            case PurchaseVIDEO:
                return "Purchase Video"
            case PurchaseRT:
                return "Purchase Ringtone"
            case FollowUnsub:
                return "Follow Unsub"
            case SentQuestion:
                return "Sent Trivia Question"
            case SentQuestionAgain:
                return "Sent Question Again"
            case ReceivedAnswer:
                return "Received Trivia Anwer"
            case SentFollowSms:
                return "Sent Follow Sms"
            case TriviaSub:
                return "Trivia Sub"
            case TriviaUnsub:
                return "Trivia Unsub"
            case TriviaAnswer:
                return "Trivia Answer"
            case TriviaHelp:
                return "Trivia Help"
        }
        return code
    }
    final static Map codesTranslations = [
            Purchase5Start:"Purchase 5 Attempt",
            Purchase10Start: "Purchase 10 Attempt",
            PurchaseBG: "Purchase BG",
            PurchaseVIDEO:"Purchase Video",
            PurchaseRT:"Purchase Ringtone",
            FollowUnsub:"Follow Unsub",
            SentQuestion:"Sent Trivia Question",
            SentQuestionAgain:"Sent Question Again",
            ReceivedAnswer:"Received Trivia Anwer",
            SentFollowSms:"Sent Follow Sms",
            TriviaSub:"Trivia Sub",
            TriviaUnsub:"Trivia Unsub",
            TriviaAnswer:"Trivia Answer",
            TriviaHelp:"Trivia Help"
    ]

}
