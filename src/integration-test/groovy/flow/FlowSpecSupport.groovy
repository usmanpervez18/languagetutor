package flow

import econ.api.test.TestSmsSender
import econ.api.test.TestStatsCollector
import org.springframework.beans.factory.annotation.Autowired
import sms.flow.FlowService

/**
 * Created by khurramijaz on 24/05/2015.
 */
trait FlowSpecSupport{
    @Autowired
    FlowService flowService

    TestStatsCollector statsCollector
    TestSmsSender sender = new TestSmsSender()

    Long from
    Long to
    int flowKey

    void flowInit(Long f , Long t , int fk){
        from = f
        to = t
        flowKey = fk
        flowService.smsSender = sender
        flowService.statsCollector = new TestStatsCollector()
        statsCollector = flowService.statsCollector as TestStatsCollector
    }

    boolean send(String text){
        sender.reset()
        return flowService.execute( from , to , text , flowKey )
    }
    boolean inState(Closure closure){
        return flowService.isInState( from, flowKey , closure )
    }
    boolean inState(String name){
        return flowService.isInState( from , flowKey , name )
    }
    String currentState(){
        return flowService.getCurrentState( from, flowKey)
    }
    boolean deleteState(){
        flowService.deleteSubscriberState( from, flowKey )
        return true
    }
    boolean inText(String text){
        return sender.text?.contains( text )
    }
}