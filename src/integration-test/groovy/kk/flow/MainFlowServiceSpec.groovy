package kk.flow

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource
import flow.FlowSpecSupport
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import kk.Subscription
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Specification
import stats.StatisticsService

@Integration
@Rollback
class MainFlowServiceSpec extends Specification implements FlowSpecSupport {

    //@Autowired
    //FlowService service
    @Autowired
    MainFlowService mainFlowService
    StatisticsService statisticsService

    def setup() {
        flowInit( 3125666666l , 5227l , 5227 )
    }

    def cleanup() {
    }


    void testSubUnSub(){

        MysqlDataSource ds = new MysqlDataSource()
        ds.url = "jdbc:mysql://localhost/spj_panel"
        ds.user = "root"
        ds.password = ""
        Sql sql = new Sql(ds )

        //def rows = sql.rows("select interval_type,count_value from stats_counter where interval_type = 3001 and count_type =1 ")

        when: "unsub for not subscribed"
        send "unsub"

        then: "should send invalid subscriber"
        inText "To Subscribe to the service send"
        Subscription.get( from ) == null

        when: "empty sms for non subscriber"
        send ""

        then: "should reply with subscribe message."
        inText( "To Subscribe to the service send")


        when: "replied with sub"
        //def rows_newSubWhen = sql.rows("select count_value from stats_counter where interval_type = 1001 and count_type =1")
        send "sub"

        then: "should present language selection msg and subscribe user."
        //def rows_newSubThen = sql.rows("select count_value from stats_counter where interval_type = 1001 and count_type =1")

        //rows_newSubWhen.size() == rows_newSubThen.size()
        //rows_newSubThen.COUNT_VALUE.toString().toInteger() == 1

        inState mainFlowService.SelectLanguage
        Subscription.get( 3125666666l ) != null
        inText "Dear User,to use Zong Science Lab Service please select your language"

        when: "replied with m"
        send "m"

        then: "should present main menu"
        inState mainFlowService.MainMenu
        inText "1. Concept of the Day"

        when: "unsub sms for subscriber"
        send "unsub"

        then: "should unsusbscribe."
        Subscription.get( 3125666666l ) == null
        inText "Dear User, you have been successfully Unsubscribed"

        when: "unsub sms for subscriber"
        send "unsub"

        then: "should send invalid subscriber"
        inText "To Subscribe to the service send"
        Subscription.get( from ) == null

        when: "unsub sms for subscriber"
        send "unsub"

        then: "should send invalid subscriber"
        inText "To Subscribe to the service send"
        Subscription.get( from ) == null

        when: "replied with sub"
        send "sub"

        then: "should present language selection msg and subscribe user."
        inState mainFlowService.SelectLanguage
        Subscription.get( 3125666666l ) != null
        inText "Dear User,to use Zong Science Lab Service please select your language"

        when: "send shortcut p"
        send "p"

        then: "should send select language"
        inText "Dear User,to use Zong Science Lab Service please select your language"
        inState "profileFlowService.main"

        when: "selects roman language"
        send "2"

        then: "should show confirmation message"
        inText "Capricorn(22 Dec - 19 Jan)"
        inState null

        when: "send shortcut p again"
        send "p"

        then: "should send DOB menu"
        inText "apna sitara muntakhib karnay ke liye 1-12"
        inState "profileFlowService.profileDOB"

        when: "replied with unsub"
        send "unsub"

        then:
        sender.text?.contains("0 likh ker reply kijye")
        inText "0 likh ker reply kijye"
        inState "mainFlowService.unsub"

        when: "replied with m"
        send "m"

        then: "forwarded to Main menu"
        inText "1. Kaisa hoga aaj ka din"
        inState mainFlowService.MainMenu
        Subscription.get( 3125666666l ) != null

        when: "replied with unsub"
        send "unsub"

        then:
        sender.text?.contains("0 likh ker reply kijye")
        inText "0 likh ker reply kijye"
        inState "mainFlowService.unsub"

        when: "replied with 0"
        send "0"

        then:
        sender.text?.contains("Aapko Zong Kismat Konnection se unsubscribe kar diya gaya hai")
        inText "Aapko Zong Kismat Konnection se unsubscribe kar diya gaya hai"
        Subscription.get( from ) == null
        inState null

        when: "empty sms for non subscriber"
        send ""

        then: "should reply with subscribe message."
        inText "Kismet Konnection mein aap maloom kar saktay hain"

        when: "replied with sub"
        send "sub"

        then: "should be subscribed"
        inState mainFlowService.MainMenu
        Subscription.get( 3125666666l ) != null

        /*when: "replied with 6"
        send "m"
        send "6"
        then: "liveKismetKaHaalService.main"
        currentState() == "liveKismetKaHaalService.main"*/

        when: "replied with 7"
        send "m"
        send "6"
        then: "should be in khoobianKhamianService.main"
        inState "khoobianKhamianService.main"

        /*when: "replied with 8"
        send "m"
        send "8"
        then: "should be in anhoniKahanianService.main"
        inState "anhoniKahanianService.main"*/

        when: "send shortcut kks"
        send "kks"

        then: "should be in kismatKeSitarayService.main"
        inState "kismatKeSitarayService.main"

        when: "send shortcut akk"
        send "akk"

        then: "should be in khoobianKhamianService.main"
        inState "khoobianKhamianService.main"

        when: "send sub"
        send "sub"

        then: "should be in mainFlowService.mainMenu"
        inState "mainFlowService.mainMenu"

        when: "send shortcut p"
        send "p"

        then: "should send DOB menu"
        inText "apna sitara muntakhib karnay ke liye 1-12"
        inState "profileFlowService.profileDOB"

        when: "selects horoscope"
        send "1"

        then: "should show confirmation message with star"
        inText "Capricorn(22 Dec - 19 Jan)"
        inState null
    }

}
